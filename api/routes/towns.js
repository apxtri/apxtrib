const express = require("express");
const path = require("path");

// Classes
const Towns = require("../models/Towns.js");
const Notifications = require("../models/Notifications.js");
// Middlewares
const checkHeaders = require("../middlewares/checkHeaders");
const isAuthenticated = require("../middlewares/isAuthenticated");
const router = express.Router();

/**
   * @api {get} /towns/ownershipr/:alias - town owner change
   * @apiName changeowner
   * @apiGroup Towns
   * @apiDescription Change owner (major role) of a town (server) after a first install or just to resale it. Only the current major can run this.
   * @param {string} alias an alias that will become owner of a town
   * @apiSuccess (object) ownerchangesuccess
   * @apiSuccessExample {json}
   * HTTP/1.1 200 OK
   * {status:200, ref:"towns",msg:"ownerchangesuccess",data: { alias } }
   * @apiError {object} aliasnotallow
   * @apiErrorExample {json}
   * HTTP/1.1 403 Forbidden
   * {status:403,ref:"towns",msg:"aliasnotallow",data: { alias} }
   *
   **/
router.get("/changeowner/:alias",checkHeaders, isAuthenticated, (req, res) => {
  res.send(Towns.changeowner(req.params.alias, req.session.header.xalias));
});

module.exports = router;
