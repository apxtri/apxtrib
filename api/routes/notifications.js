const express = require("express");
// Classes
const Notifications = require("../models/Notifications.js");

// Middlewares
const checkHeaders = require("../middlewares/checkHeaders");
const isAuthenticated = require("../middlewares/isAuthenticated");

const router = express.Router();

/**
 * @api {get} /notifications/:alias/:tribeId 
 * @apiName notiflist
 * @apiDescription Get list of notifications for an alias and a tribe 
 * @apiGroup Notification
 *
 * @apiParam {string} alias
 * @apiParam {string} tribeId
 * @apiSuccess {object}  notif content
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {status:200,ref:"Notification",msg:"Messagelist",data:{notif:[{tribeId,msg:{from(email or uuid or alias):[{dt,msg}]}}]}
 * 
 **/
router.get("/:alias/:tribeId", (req, res) => {
  const getnot = Notification.get(req.params.alias,req.params.tribeId);
  res.status(getalias.status).send(getalias);
});

module.exports=router;