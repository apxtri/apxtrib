const express = require( 'express' );
const fs=require('fs-extra');
const conf = require(`${process.env.dirtown}/conf.json`);

// Classes
const Nations = require( '../models/Nations.js' );
// Middlewares
const checkHeaders = require( '../middlewares/checkHeaders' );
const isAuthenticated = require( '../middlewares/isAuthenticated' );
const router = express.Router();

/*
Manage nation 
A major create a nation with at least a town => nation:{name, towns:[]}  contracts/nationname.js + contracts/townsname.js 
Manage a new towns in a nation => update nation:[nationname:towns:[]} contracts/townname.js
*/


/**
 * @api {put} /nations/viewtown/:town - nationlist from a town major
 * @apigroup Nation
 * @apiName nationlist
 * @apiDescription get list of nation from a town to help this instance to update network topology
 * @apiParam {string} town fromwhich the data come from
 * @apiSuccess {object} refreshnetworktopoly
 *  * HTTP/1.1 200 OK
 * {status:200,ref:"Nations",msg:"updated",data:{}};
 * 
 */
router.put( '/', checkHeaders, isAuthenticated, ( req, res ) => {
    const uptown = Nations.updatetown()
	res.status(uptown.status).send(uptown)
} )

module.exports = router;
