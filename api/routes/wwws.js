const express = require("express");
const path = require("path");

// Classes
const Wwws = require("../models/Wwws.js");
// Middlewares
const checkHeaders = require("../middlewares/checkHeaders");
const isAuthenticated = require("../middlewares/isAuthenticated");

const router = express.Router();
// GET api/wwws/conf/:tribeId/:website
// if profils accessright return the nginx conf in ${conf.dirtown}/tribes/${req.param.tribeId}/www/nginx_${req.params.tribeId}_${req.params.website}.conf
router.get("/conf/:tribeId/:website", checkHeaders, isAuthenticated, (req, res) => {
  res.send(Www.configlist(req.params.tribeId));
});
router.post("/conf/:tribeId/:website", checkHeaders, isAuthenticated, (req, res) => {
  res.send(Wwws.create(req.params.tribeId));
});
module.exports = router;
