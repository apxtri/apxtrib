const express = require("express");
const glob = require("glob");
const fs = require("fs-extra");
const path = require("path");
const conf = require(`${process.env.dirtown}/conf.json`);
const Odmdb = require("../models/Odmdb.js");
// Middlewares
const checkHeaders = require("../middlewares/checkHeaders");
const isAuthenticated = require("../middlewares/isAuthenticated");
const router = express.Router();


/**
 * @api {get} https://dns.xx/trk/pathtofile? - tracking system
 * @apiGroup Trackings
 * @apiName trackingsystem
 * @apiDescription   
 *  without header:<br>
 * <code>https://dns.xx/trk/pathtofile?alias=anonymous&uuid=1b506f71-1bff-416c-9057-cb8b86296f60&src=btnregister&version=1&lg=fr </code>
 * 
 * with header<br>
 * <code>https://dns.xx/trk/pathtofile?srckey=btnregister&version=1</code>
 * 
 * where pathtofile is a ressource accessible from  https://dns.xx/pathtofile 
* html usage to track a loading page or email when a picture is load
 * using apxwebapp in /src/ we got:
 * <code> < img src="static/img/photo.jpg" data-trksrckey="loadpage" data-version="1"  > </code>
 * 
 * using html + apx.js (or at least with header {xalias,xuuid,xlang})
 * <code>< img lazysrc="trk/static/img/photo.jpg data-trksrckey="loadpage" data-version="1" ></code>
 * 
 * in js action:
 * 
 * <code> 
 *   <button></button>
 *   <a data-trksrckey="linktoblabla" href='https:..'
 *    onclick="apx.trackvisit("btnaction",1);actionfct();">
 *   </a>
 * </code>  
 * #will hit an eventlistener<br>
 * <code> axios.get("https://dns.xx/trk/cdn/empty.json?alias=anonymous&uuid=1b506f71-1bff-416c-9057-cb8b86296f60&srckey=btnregister&version=1");
 * </code>
 *  
 * #or if no js available (example:email or pdf document)<br>
 * <code> < img src="https://dns.xx/trk/static/img/photo.jpg?alias=anonymous&uuid=1b506f71-1bff-416c-9057-cb8b86296f60&srckey=loadpage&version=1"</code> 
 *  
 * <code>
 * <a href="https://dns.xx/trk/redirect?alias=anonymous&uuid=1b506f71-1bff-416c-9057-cb8b86296f60&srckey=loadpage&version=1&url=http://..."
 * </code>
 *  
 * will hit a tracker then redirect to url></a> * 
 * 
 * 
 * 
 * **if you use apx.js** : in html add in < button >, < img >, < a > tag data-trksrc="srckey"
 * <code>
 *  < img src="https://dns.xx/static/img/photo.jpg" data-trkversion="1" data-trksrckey="registerform">
 *  < button data-trksrc="https://dns.xx/static/img/photo.jpg" data-trkversion="1" data-trksrckey="registerform">
 * </code>
 * 
 * Tracking log are store into tribe/logs/nginx/tribe_appname.trk.log
 * Src have to be manage in tribe/api/models/lg/src_en.json
 * <code>{"srckey":{
 *        "app":"presentation|app|apptest",
 *        "title":"",
 *        "description":""
 *      }
 * }
 * </code>
 *
 * @apiParam {String} alias=anonymous if authenticated we get from headers
 * @apiParam {String} uuid a uuid v4 generate the first time a web page is open on a browser
 * @apiParam {String} srckey  source action that trig this get
 * @apiParam {integer} version=1 can be an int, date or any version of the src
 * @apiParam {integer} [tm] a timestamp of action when it is not immediate (for offline app)
 *  
 */




module.exports=router;