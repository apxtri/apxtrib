const fs = require("fs-extra");
const path = require("path");
const dnsSync = require("dns-sync");
const mustache = require("mustache");
const readlineSync = require("readline-sync");
const conf = require(`${process.env.dirtown}/conf.json`);
const Wwws = {};

/*Wwws.apxtribinstall = (paramconf) => {
  if (fs.existsSync(`${conf.dirtown}/conf.json`)) {
    console.log("You already have a conf on this town");
    process.exit();
  }
  //first install
  const nginxconf = fs.readFileSync(
    `${conf.dirapi}/adminapi/www/adminapx/conf/nginx.conf.mustache`,
    "utf8"
  );
  const proxyparams = fs.readFileSync(
    "../../nationchains/www/adminapx/static/tpl/nginxproxy_params.mustache",
    "utf8"
  );
  // saved and change nginx conf
  if (!fs.existsSync("/etc/nginx/nginxconf.saved")) {
    fs.moveSync("/etc/nginx/nginx.conf", "/etc/nginx/nginxconf.saved");
    console.log(
      "your previous /etc/nginx/nginx.conf was backup in  /etc/nginx/nginxconf.saved"
    );
  }
  fs.outputFileSync(
    "/etc/nginx/nginx.conf",
    mustache.render(nginxconf, paramconf),
    "utf8"
  );
  fs.outputFileSync(
    "/etc/nginx/proxy_params",
    mustache.render(proxyparams, paramconf),
    "utf8"
  );
  if (!fs.existsSync(paramconf.nginx.logs)) fs.mkdirSync(paramconf.nginx.logs);
  paramconf.nginx.firstinstall = true;
  fs.outputJsonSync("../../nationchains/tribes/conf.json", paramconf, {
    space: 2,
  });

  return Www.create(paramconf.nginx);
};
*/
Wwws.create = (paramnginx) => {
  /**
   * Create an nginx conf to make available a spaceweb for a tribe /www/appname/
   * 
   */
  const res = {
    status: 200,
    ref: "Www",
    msg: "successfulwww",
    data: { website: paramnginx.website },
  };
  const nginxwebsite = fs.readFileSync(
    `${conf.dirapi}/adminapi/www/adminapx/conf/nginxmodelwebsite.conf.mustache`,
    "utf8"
  );
  fs.outputFileSync(
    `./${paramnginx.fswww}www/nginx_${paramnginx.website}.conf`,
    mustache.render(nginxwebsite, paramnginx),
    "utf8"
  );
  if (!fs.existsSync(`./${paramnginx.fswww}www/${paramnginx.website}`)) {
    //See later how to generate specific template of webapp
    fs.mkdirSync(`./${paramnginx.fswww}www/${paramnginx.website}`);
  }
  if (!fs.existsSync(`./${paramnginx.fswww}www/cdn`)) {
    //See later how to generate specific template of webapp
    fs.mkdirSync(`./${paramnginx.fswww}www/cdn`);
  }
  //restart nginx
  const { exec } = require("child_process");
  exec(paramnginx.restart, (error, stdout, stderr) => {
    if (error) {
      if (paramnginx.firstinstall) {
        console.log("\x1b[42m", error, stdout, stderr, "x1b[0m");
      }
      //@todo supprimer la derniere config et relancer
      res.status = 500;
      res.msg = "nginxError";
      res.data = { msg: `${error}<br>${stdout}<br>${stderr}` };
    } else {
      if (paramnginx.firstinstall) {
        // the tribes/conf.json is saved in apxtribinstall
        console.log(
          `\x1b[42m###########################################################################################\x1b[0m\n\x1b[42mWellcome into apxtrib, you can now 'yarn dev' for dev or 'yarn startpm2' for prod or       \n'yarn unittest' for testing purpose. Access to your town here \x1b[0m\x1b[32mhttp://adminapx\x1b[0m \x1b[42m             \nto finish your town setup. Don't forget to set your localhost /etc/hosts by adding 127.0.0.1 adminapx or {LAN IP} adminapx .  Check README's project to learn more.                          \x1b[0m\n\x1b[42m###########################################################################################\x1b[0m`
        );
      } else {
        // add website to tribe conf
      }
    }
  });
  return res;
};
Wwws.setssl = () => {
  // Run process to change nginx conf to get a ssl
};

Wwws.configlist = (tribeId) => {
  //if accessright R return list of conf parameter {webapp:{conf parameter}}
  const res = { status: 200, data: {} };
  return res;
};

module.exports = Wwws;
