const glob = require("glob");
const path = require("path");
const fs = require("fs-extra");
const dayjs = require("dayjs");
const axios = require("axios");
const conf = require(`${process.env.dirtown}/conf.json`);
const Checkjson = require(`./Checkjson.js`);

const Trackings = {}

/**
 * Tracking system management
 * 
 * Data collection is done from nginx log system see routes/trackings.js for doc
 */


/**
 * Process plan to run each night or on demand to collect log data and cleanup
 */
Trackings.logcollection=()=>{

}

Trackings.dashboard=(graphname)=>{
    console.log('Process data to provide a specific graph')
}

module.export = Trackings;