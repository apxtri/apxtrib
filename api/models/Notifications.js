const glob = require("glob");
const path = require("path");
const fs = require("fs-extra");
const axios = require("axios");
//const smtp = require("smtp-client");
const nodemailer = require("nodemailer");
const conf = require(`${process.env.dirtown}/conf.json`);
/**
 * To manage any communication between Pagan
 * mayor druid emailing/sms/paper from tribe register smtp, simcard, mail api to Person(s) / Pagan(s)
 * volatile notification message from tribe activities to Pagans / person ()
 *
 */
const Notifications = {};

Notifications.get = (alias, tribeId) => {
  const notiffile = `${conf.dirtown}/tribes/${req.params.tribeId}/notifications/${req.params.alias}.json`;
  const msg = fs.existsSync(notiffile) ? fs.readJSONSync(notiffile) : {};
  return {
    status: 200,
    ref: "Notification",
    msg: "Messagelist",
    data: { notif: [{ tribeId, msg }] },
  };
};

Notifications.sendsms = async (data, tribeId) => {
  /**
   * Never use need wallet in mailjet to test
   * To set up with mailjet see https://dev.mailjet.com/sms/guides/send-sms-api/#authentication
   *
   * @param {string} data.To a phone number with international +3360101010101
   * @param {string} data.Text text to send
   *
   * a conf.sms with {url:"smsurl", Token:"", From:""}
   *
   *
   */

  if (!conf.sms) {
    return {
      status: 412,
      ref: "Notifications",
      msg: "missingconf",
      data: { tribe: tribeId },
    };
  }
  let missingk = [][("To", "Text")].forEach((k) => {
    if (!data[k]) {
      missingk.push(k);
    }
  });
  if (missingk.lenght > 0) {
    return {
      status: 428,
      ref: "Notifications",
      msg: "missingdata",
      data: { missingk: missingk },
    };
  }
  let confsms = conf.sms;
  if (
    fs.existsSync(
      `${process.env.dirtown}/tribes/itm/${req.session.header.xtribe}.json`
    )
  ) {
    const conftrib = fs.readJSONSync(
      `${process.env.dirtown}/tribes/itm/${req.session.header.xtribe}.json`
    );
    if (conftrib.sms) confsms = conftrib.sms;
  }
  data.From = confsms.From;
  const sendsms = await axios.post(confsms.url, {
    headers: {
      Authorization: `Bearer ${confsms.MJ_TOKEN}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  if (sendsms.status == 200) {
    return {
      status: 200,
      ref: "Notifications",
      msg: "successfullsentsms",
      data: {},
    };
  } else {
    return {
      status: sendsms.status,
      ref: "Notifications",
      msg: "errsendsms",
      data: { err: sendsms.data },
    };
  }

  /* si tout se passe bien:
  {
  "From": "MJPilot",
  "To": "+33600000000",
  "Text": "Have a nice SMS flight with Mailjet !",
  "MessageId": "2034075536371630429",
  "SmsCount": 1,
  "CreationTS": 1521626400,
  "SentTS": 1521626402,
  "Cost": {
    "Value": 0.0012,
    "Currency": "EUR"
  },
  "Status": {
    "Code": 2,
    "Name": "sent",
    "Description": "Message sent"
  }
}
}
*/
};

Notifications.sendmail = async (data, tribe) => {
  /**
   * @param {string} [data.from] an email authorized by smtp used priority from header xtribe
   * @param {string} data.to list of email separate by ,
   * @param {string} data.subject
   * @param {string} data.html
   * @param {string} data.text
   * @param {string} [data.Cc] list of email in copy
   * @param {string} [data.Bcc] list of email in hidden copy
   * @param {string} [data.filelist} an array of object {filename:"",pathfile:"",filetype:""} pathfile to attach as file name of type:filetype "filename" to this email
   * example of filetype : "text/plain", "text/csv", image/gif", "application/json", "application/zip"
   *
   * @example data
   * {"to":"wall-ants.ndda.fr",
   *  "subject":"Test",
   *  "html":"<h1>test welcome</h1>",
   *  "text":"test welcome",
   *  "attachments":[{filename:"text.txt",pathfile:"/media/phil/textA.txt","contenttype":"text/plain"}]
   *  }
   * @return {object}
   *  { status: 200, ref:"pagans",msg:"aliasexist",data: { alias, publicKey } }
   *
   *
   */
  if (!conf.smtp || !conf.emailcontact) {
    return {
      status: 412,
      ref: "Notifications",
      msg: "missingconf",
      data: { tribe: tribe },
    };
  }
  if (!data.from) {
    data.from = conf.emailcontact;
  }
  let missingk = [];
  ["from", "to", "subject", "html", "text"].forEach((k) => {
    if (!data[k]) {
      missingk.push(k);
    }
  });
  if (missingk.lenght > 0) {
    return {
      status: 428,
      ref: "Notifications",
      msg: "missingdata",
      data: { missingk: missingk },
    };
  }
  let confsmtp = conf.smtp;
  const conftribfile = `${process.env.dirtown}/tribes/itm/${tribe}.json`;
  if (fs.existsSync(conftribfile)) {
    const conftrib = fs.readJSONSync(conftribfile);
    confsmtp = conftrib.smtp;
    data.from = conftrib.emailcontact;
  }

  //const client = smtp.connect(confsmtp);
  const transporter = await nodemailer.createTransport(confsmtp);
  //@todo add attachments management
  if (data.filelist) {
    let missingfile = [];
    data.filelist.forEach((fo) => {
      if (fs.existsSync(fo.pathfile)){

      }else{ missingfile.push(fo.pathfile);}
    });
    if (missingfile.lenght > 0)
      return {
        status: 428,
        ref: "Notifications",
        msg: "missingfile",
        data: { missingfile: missingfile },
      };
  }
  console.log("data:", data);
  const res = await transporter.sendMail(data);
  if (
    res.accepted &&
    data.to.split(",").reduce((acc, m) => acc && res.accepted.includes(m), true)
  ) {
    data.accepted = res.accepted;
    data.rejected = res.rejected;
    return {
      status: 200,
      ref: "Notifications",
      msg: "successfullsentemail",
      data,
    };
  } else if (res.accepted && res.rejected) {
    data.accepted = res.accepted;
    data.rejected = res.rejected;
    return { status: 410, ref: "Notifications", msg: "errsendmail", data };
  } else {
    data.errmailer = res.err;
    return { status: 417, ref: "Notifications", msg: "errsendmail", data };
  }
};

module.exports = Notifications;
