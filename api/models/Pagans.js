const glob = require("glob");
const path = require("path");
const dayjs = require("dayjs");
const fs = require("fs-extra");
const axios = require("axios");
const Mustache = require('mustache');
const openpgp = require("openpgp");
const Notifications = require("../models/Notifications.js");
const Odmdb = require("../models/Odmdb.js");

const conf = require(`${process.env.dirtown}/conf.json`);

/**
 * Pagan Management numeric Identity and Person  (Person = Pagan Id + tribe)
 *
 *
 *
 */
const Pagans = {};

/**
 *  Remove authentification token after a logout
 * @param {string} alias
 * @param {string} tribe
 * @param {integer} xdays
 * @param {string} xhash
 * @returns {status:200, ref:"Pagans",msg:"logout"}
 * tmpfs name file has to be on line with the tmpfs create by isAuthenticated
 *  tmpfs contain profils name for a tribe/
 */
Pagans.logout = (alias, tribe, xdays, xhash) => {
  //console.log(alias, tribe, xdays, xhash);
  // inline with middleware isAuthenticated.js
  let tmpfs = `${process.env.dirtown}/tmp/tokens/${alias}_${tribe}_${xdays}`;
  //max filename in ext4: 255 characters
  tmpfs += `_${xhash.substring(150, 150 + tmpfs.length - 249)}.json`;
  fs.remove(tmpfs);
  console.log('logout token',tmpfs)
  return { status: 200, ref: "Pagans", msg: "logout" };
};

/**
 * @param {string} alias a alias that exist or not
 * @return {object}  { status: 200, ref:"pagans",msg:"aliasexist",data: { alias, publicKey } }
 *                   { status: 404, ref:"pagans",msg:"aliasdoesnotexist",data: { alias} }
 *
 **/
Pagans.getalias = (alias) => {
  //bypass Odmdb cause all is public save ressources
  if (fs.existsSync(`${conf.dirapi}/nationchains/pagans/itm/${alias}.json`)) {
    return {
      status: 200,
      ref: "Pagans",
      msg: "aliasexist",
      data: fs.readJSONSync(
        `${conf.dirapi}/nationchains/pagans/itm/${alias}.json`
      ),
    };
  } else {
    return {
      status: 404,
      ref: "Pagans",
      msg: "aliasdoesnotexist",
      data: { alias },
    };
  }
};


/**
 * Send email with alias's keys to email or person alias person.recovery.email
 *
 * If email or pubkey is undefined then get data from tribe/person(alias)
 * Send email with keys
 *
 * @param {string} alias
 * @param {pgpPrivate} privkey
 * @param {string} passphrase
 * @param {string} tribe
 * @param {pgpPublic} pubkey
 * @param {string} email
 */
Pagans.sendmailkey = (
  alias,
  privatekey,
  tribe,
  passphrase,
  publickey,
  email,
  lg
) => {
  const person = { alias, privatekey, tribe };
  console.log(
    alias,
    "-",
    privatekey.substring(0,10),
    "-",
    tribe,
    "-",
    passphrase,
    "-",
    publickey.substring(0,10),
    "-",
    email
  );

  if (!publickey || !email || !privatekey) {
    const personfile = `${process.env.dirtown}/tribes/${tribe}/objects/persons/itm/${alias}.json`;
    if (!fs.existsSync(personfile)) {
      return {
        status: 404,
        ref: "Pagans",
        msg: "persondoesnotexist",
        data: { alias, tribe },
      };
    }
    const persondata= fs.readJsonSync(personfile);
    person.email = persondata.recoveryauth.email;
    person.publickey = persondata.recoveryauth.publickey;
    person.privatekey = persondata.recoveryauth.privatekey;
    person.passphrase = persondata.recoveryauth.passphrase;
  } else {
    person.email = email;
    person.passphrase = passphrase;
    person.publickey = publickey;
  }
  person.avecpassphrase=(person.passphrase!="")
  console.log("person:", person);
  const tplemail = require(`${conf.dirapi}/adminapi/template/createidentity_${lg}.js`)

  person.message 
  const maildata = {
    to: person.email,
    subject: Mustache.render(tplemail.subject, person),
    html: Mustache.render(tplemail.html, person),
    text: Mustache.render(tplemail.text, person),
    attachments: [
      {
        filename:`${person.alias}_publickey.txt`,
        content: person.publickey,
        contentType:"text/plain"
      },
      {
        filename:`${person.alias}_privatekey.txt`,
        content: person.privatekey,
        contentType:"text/plain"
      }
    ]
  };
  return Notifications.sendmail(maildata, tribe);
};

Pagans.authenticatedetachedSignature = async (
  alias,
  pubK,
  detachedSignature,
  message
) => {
  /**
   *  Check that a message was signed with a privateKey from a publicKey
   *  This is not necessary if isAuthenticated, but can be usefull to double check
   *  @TODO finish it and implement it also in  /apxpagan.js for browser
   *  @alias {string} alias link to the publicKey
   *  @pubK {string} publiKey text format
   *  @detachedSignature  {string} a detachedsignatured get from apx.detachedSignature
   *  @message {string} the message signed
   *  @return {boolean} true the message was signed by alias
   *                    false the message was not signed by alias
   */
  const publicKey = await openpgp.readKey({ armoredKey: pubK });
  const msg = await openpgp.createMessage({ text: message });
  const signature = await openpgp.readSignature({
    armoredSignature: detachedSignature, // parse detached signature
  });
  const verificationResult = await openpgp.verify({
    msg, // Message object
    signature,
    verificationKeys: publicKey,
  });
  const { verified, keyID } = verificationResult.signatures[0];
  try {
    await verified; // throws on invalid signature
    console.log("Signed by key id " + keyID.toHex());
    return KeyId.toHex().alias == alias;
  } catch (e) {
    console.log("Signature could not be verified: " + e.message);
    return false;
  }
};

Pagans.keyrecovery = (tribeid, email) => {
  glob
    .GlobSync(`${conf.dirtown}/tribes/${tribeId}/Person/*.json`)
    .forEach((f) => {
      const person = fs.readJsonSync(f);
      if (person.recoveryauth && person.recoveryauth.email) {
        // send email (alias publickey privatekey )
      }
    });
  return { status: 200, ref: "Pagans", msg: "todo" };
};
module.exports = Pagans;
