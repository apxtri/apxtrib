/*
Unit testing
*/
const assert = require("assert");
const Checkjson = require("../Checkjson.js");
const conf = require(`${process.env.dirtown}/conf.json`);

const ut = { name: "Checkjson" };

const schema = {
  $schema: "http://json-schema.org/schema#",
  title: "Dummy schema to test Checkjson.js",
  description: "Checkjson is use on server as well as into a browser",
  $comment: "We change schema type on the fly to simplify the test",
  type: "object",
  properties: {
    totest: {},
  },
};
const testproperties = [
  {
    name: "test0",
    data: { totest: true },
    properties: { totest: { type: "boolean" } },
    status: 200,
  },
  {
    name: "test1",
    data: { totest: "blabla" },
    properties: { totest: { type: "string" } },
    status: 200,
  },
  {
    name: "test2",
    data: { totest: 123 },
    properties: { totest: { type: "string" } },
    status: 417,
  },
  {
    name: "test3",
    data: { totest: 123.13 },
    properties: { totest: { type: "integer" } },
    status: 417,
  },
  {
    name: "test4",
    data: { totest: 123 },
    properties: { totest: { type: "number" } },
    status: 200,
  },
  {
    name: "test5",
    data: { totest: 12312 },
    properties: { totest: { type: "number" } },
    status: 200,
  },
  {
    name: "test6",
    data: { totest: 12.313 },
    properties: { totest: { type: "float" } },
    status: 200,
  },
  {
    name: "test7",
    data: { totest: "blablab sfde" },
    properties: { totest: { type: "string", minLength: 1111 } },
    status: 417,
  },
  {
    name: "test8",
    data: { totest: "blablab sfde" },
    properties: { totest: { type: "string", minLength: 4, maxLength: 128 } },
    status: 200,
  },
  {
    name: "test9",
    data: { totest: 12 },
    properties: { totest: { type: "integer", multipleOf: 3 } },
    status: 200,
  },
  {
    name: "test10",
    data: { totest: 9 },
    properties: {
      totest: { type: "number", minimum: -10, exclusiveMaximum: 10 },
    },
    status: 200,
  },
  {
    name: "test11",
    data: { totest: 10 },
    properties: {
      totest: { type: "number", minimum: -10, exclusiveMaximum: 10 },
    },
    status: 417,
  },
  {
    name: "test12",
    data: { totest: "gfhrtabcdgfr" },
    properties: { totest: { type: "string", pattern: /.*abc.*/ } },
    status: 200,
  },
  {
    name: "test13",
    data: { totest: "toto@google.com" },
    properties: { totest: { type: "string", format: "email" } },
    status: 200,
  },
  {
    name: "test14",
    data: { totest: "Aze123@0" },
    properties: { totest: { type: "string", format: "password" } },
    status: 200,
  },
  {
    name: "test15",
    data: { totest: "value1" },
    properties: {
      totest: { type: "string", enum: ["value1", "value2", "value3"] },
    },
    status: 200,
  },
  {
    name: "test16",
    data: { totest: ["t1", "t2"] },
    properties: { totest: { type: ["string", "number"] } },
    status: 417,
  },
  {
    name: "test17",
    data: { totest: 12 },
    properties: { totest: { type: ["string", "number"] } },
    status: 200,
  },
];

ut.testproperties = (options) => {
  let msg = "";
  testproperties.forEach((t) => {
    schema.properties = t.properties;
    const res = Checkjson.schema.data(schema, t.data);
    if (res.status != t.status) {
      msg = msg == "" ? "Unconsistent testproperties() name list: " : `${msg},`;
      if (options.verbose) {
        console.log(t);
        console.log(res);
      }
      msg += res.err.map((e) => ` ${t.name} ${e.info}`);
    }
  });
  return assert.deepEqual(msg, "", msg);
};

ut.run = (options) => {
  console.log("Test Checkjson properties");
  ut.testproperties(options);
};
module.exports = ut;
