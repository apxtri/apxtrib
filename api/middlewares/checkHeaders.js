const conf = require(`${process.env.dirtown}/conf.json`);
  /**
   * @api {get} http://header/CheckHeaders  - CheckHeaders  
   * @apiGroup Middlewares
   * @apiName CheckHeaders
   * @apiDescription a list of headers are mandatory to access apxtrib see in your space town /conf.json.exposedHeaders
   *
   * @apiHeader {string} xalias 'anonymous' or unique alias
   * @apiHeader {string} xapp  name of the webapp store in tribe/tribeid/www/{xapp}
   * @apiHeader {string} xlang the 2 letter request langage (if does not exist then return en = english).
   * @apiHeader {string} xtribe unique tribe name where xapp exist
   * @apiHeader {string} xdays a timestamp 0 or generate during the authentifyme process
   * @apiHeader {string} xhash anonymous or signature of message: xalias_xdays created by alias private key during authentifyme process
   * @apiHeader {array[]} xprofils list of string profil apply into xtribe for xapp
   * @apiHeader {string} xuuid a unique number uuid.v4 created the fisrt time a domain is visited on a device
   * @apiHeader {integer} xtrkversion a version number link to tracking system
   *
   * @apiHeaderExample {json} Header-Example:
   *  {
   *       Cache-Control: "no-cache",
   *       Expires: 0, Pragma:"no-cache",
   *       xalias:"jojo",
   *       xapp:"presentation",
   *       xdays:1700733068298
   *       xhash:"LS0tLS1CRUdJTiBQR1AgU0lHTkVEIE1FU1NBR0UtLS0tLQpIYXNoOiBTSEE1MTIKCmpvam9fMTcwMDczMzA2ODI5OAotLS0tLUJFR0lOIFBHUCBTSUdOQVRVUkUtLS0tLQoKd25VRUFSWUtBQ2NGZ21WZklJd0prTmFVQ0daRHVUYnBGaUVFTjZlc1ZMSWdURmtPRGFVaDFwUUlaa081Ck51a0FBR09MQVA5OS96c21YeEd0b0VuYnpnekppZDJMcDA3YlBNZ1gwNUdhOUFVWjlCQm91Z0VBOVlYVworYjZIM2JHWHVhbEVOc3BrdUk1alNlTFNUWGNkSStjTExTZk5OQTg9Cj1uVjhNCi0tLS0tRU5EIFBHUCBTSUdOQVRVUkUtLS0tLQo=",
   *       xlang:"fr",
   *       xprofils:["anonymous", "pagans"],
   *       xtribe:"smatchit",
   *       xtrkversion:1,
   *       xuuid:"ea1cf73f-27f5-4c69-ab53-197a0feab9b2"
   * 	}
 * @apiErrorExample {json} Error-Response:
 * 	HTTP/1/1 400 Not Found
 * 	{
 * 	  	status:400,
 * 			ref:"middlewares",
 * 			msg:"missingheaders",
 *      data:["headermissing1"]
 *  }
 * @apiErrorExample {json} Error-Response:
 * 	HTTP/1/1 404 Not Found
 * 		{
 * 			status:404,
 * 			ref:"middlewares"
 * 			msg:"tribeiddoesnotexist",
 *      data: {xalias}
 *      }
 */
const checkHeaders = (req, res, next) => {
  req.session = {};
  const header = {};
  if (!req.header("xlang") && req.header("Content-Language"))
    req.params.xlang = req.header("Content-Language");
  let missingheader = [];
  //console.log("req.headers", req.headers);
  for (const h of conf.api.exposedHeaders) {
    //console.log( h, req.header( h ) )
    if (req.params[h]) {
      header[h] = req.params[h];
    } else if (req.header(h)) {
      header[h] = req.header(h);
    } else {
      missingheader.push(h);
    }
  }
  // console.log( 'pass header', header )
  // store in session the header information
  req.session.header = header;
  // Each header have to be declared
  if (missingheader != "") {
    // bad request
    return res.status(400).json({
      status:400,
      ref: "middlewares",
      msg: "missingheader",
      data: missingheader,
    });
  }
  //console.log( req.app.locals.tribeids )
  // xtribe == "town" is used during the setup process
  // xtribe == "adminapi" is used to access /adminapi
  if (
    !(
      ["town","adminapi"].includes(header.xtribe) || req.app.locals.tribeids.includes(header.xtribe)
    )
  ) {
    return res.status(404).json({
      status:404,
      ref: "middlewares",
      msg: "tribeiddoesnotexist",
      data: { xtribe: header.xtribe },
    });
  }
  if (!conf.api.languages.includes(header.xlang)) {
    console.log("warning language requested does not exist force to english");
    header.xlang = "en";
  }
  //set anonymous profil
  req.session.header.xprofils=["anonymous"]
  next();
};
module.exports = checkHeaders;
