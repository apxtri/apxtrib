## api users and backend developers

api documentation for routes and middleware has to respect apidoc's rules [https://apidocjs.com/](https://apidocjs.com) 

To update this doc accessible in [https://wal-ants.ndda.fr/cdn/apidoc](https://wal-ants.ndda.fr/cdn/apidoc) :

`yarn apidoc` 

For api tribe's doc  accessible in [https://smatchit.io/cdn/apidoc](https://smatchit.io/cdn/apidoc) [:](https://smatchit.io/cdn/apidoc:) 

`yarn apidoctribename`

Objects manage in apXtrib: pagans, notifications, nations, towns, tribes, wwws 

All others objects are manage in town/tribe 

```plaintext
/apxtrib/                          # core process 
/townName_nationName/conf.json     # town settings contain all glabl parameter
```

url: **/api/routeName** For core api apXtrib in /apxtrib :

```plaintext
/apxtrib/api/middlewares/
/apxtrib/api/routes/
/apxtrib/api/models/
/apxtrib/api/models/lg/ lauage accessible by https://wall-ants.ndda.fr/nationchains/models/Checkjson_fr.json
/apxtrib/api/models/unitest/
```

url: **/api/smatchit/routeName** for tribe smatchit example api in /town\_nation/tribes/smatchit(tribeid) 

```plaintext
/town_nation/tribes/smatchit/api/routes/
/town_nation/tribes/smatchit/api/models/
/town_nation/tribes/smatchit/api/models/lg/ language  customization  accessible  https://smatchit.io/smatchit/models/model_lg.json 
```

**static files** are served by nginx, each tribe nginx conf are store and can be customize in /town\_nation/www/nginx\_xtribe\_xapp.conf

## Object management (Odmdb)

An object has a name and is defined by a schema that contain properties key.

A propertie has a name and a list of caracteristics (type, pattern,format,...) that have to be validate to be accepted.  
All properties respect the rules [https://json-schema.org/draft/2020-12/schema,](https://json-schema.org/draft/2020-12/schema,) some extra"format" can be add to mutualise recurrent regex pattern

To access a schema [https://wall-ants.ndda.fr/nationchains/schema/nations.json](https://wall-ants.ndda.fr/nationchains/schema/nations.json) and language specifique [https//:wall-ants.ndda.fr/nationchains/schema/lg/nations\_fr.json](https//:wall-ants.ndda.fr/nationchains/schema/lg/nations_fr.json)

A checkjson.js is available to manage all specific format [https://wall-ants.ndda.fr/Checkjson.js](https://wall-ants.ndda.fr/Checkjson.js) see **Odmdb - schema Checkjson**

**Additional properties that not exist in 2020-12/schema :**

**required**: an array of required properties

**apxid**: the propertie used as an unique id

**apxuniquekey**: array of unique properties

**apxidx** : array of index

**apxaccessrights**: object with key profilname and accessrights on properties {profilname:{C:\[properties array\],R:\[properties array\],U:\[\],D:\[\]}}

Items of an object are store in files into :  

```plaintext
/objectnames/idx/keyval_objkey.json
/objectnames/itm/uniqueid.json
```

## api pre-request

**Valid header see Middlewares**

App use openpgp.js lib to sign xdays\_xalias with a privatekey and store it in xhash.

/api/middlewares/isAuthenticated.js check if (xhash) is a valid signature of the public key a xhash is valid for 24 hours

See Pagans models that contain authentification process

**api Return in 3 data structure:**

A - data file from a classical get  [https://wall-ants.ndda.fr/Checkjson.js](https://smatchit.io/Checkjson.js)

B -  a json single answer {status, ref,msg,data}:

*   status: http code return
*   ref: model/route name reference where message come from
*   msg: a message template key store into models/lg/name\_lg.json (where lg is 2 letters language)
*   data: an object data use to render the value of the message key.

C - a json multi answer {status,multimsg:\[{ref,msg,data}\]}

         Each {ref,msg,data\] work the same way than B

To show feedback context message in a language lg => get /nationchains/models/{{ref}}\_{{lg}}.json  
This contain a json {msg:"mustache template string to render with data"}  

## Accessrights:

An alias is just an identity, to access a tribe, a person must exist with an authenticated alias into /town/tribes/tribename/persons/itm/alias.json

A person has a property profils with a list of profilename, common profiles are : anonymous (no identity) / pagan (an identity)  / person (an identity with access right into a tribe) / druid (the administrator of a tribe) / major (administrator of a town/server)

Each object has an apxaccessrights that is a list of profil and CRUD access per object key .

## Add tribe's api:

Accessible with https://dns/api/tribename/routes

```plaintext
/town/tribes/tribename/api/routes
/town/tribes/tribename/api/middlewares
/town/tribes/tribename/api/models
/town/tribes/tribename/schema
/town/tribes/tribename/schema/lg
```

```plaintext
// Example of a route
const conf = require(`${process.env.dirtown}/conf.json`);
const express = require(`${conf.dirapi}/node_modules/express`);
const fs = require(`${conf.dirapi}/node_modules/fs-extra`);
const path = require(`${conf.dirapi}/node_modules/path`);

const Nofications = require(`${conf.dirapi}/api/models/Notifications.js`);

// Middlewares
const checkHeaders = require(`${conf.dirapi}/api/middlewares/checkHeaders`);
const isAuthenticated = require(`${conf.dirapi}/api/middlewares/isAuthenticated`);
const Actions = require(`${conf.dirtown}/tribes/smatchit/api/models/Actions`);
const router = express.Router();
```