module.exports = {
	"extends": "eslint:recommended",
	"rules": {
		"@typescript-eslint/no-unused-vars": "off",
		"arrow-body-style": "off"
	},
	"env": {
		"browser": true,
		"es6": true,
		"node": true
	},
	"parserOptions": {
		"ecmaVersion": "latest"
	}
};
