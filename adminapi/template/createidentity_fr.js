// See https://nodemailer.com/message/ for available fields to add
// email template to get key registration
const tplemail={};
tplemail.sender = "smatchtit<noreply@smatchit.io>"
tplemail.replyTo="{{{name}}}<{{{email}}}>"
tplemail.inreplyTo=""
tplemail.references="" 
//tplemail.from = "noreply@smatchit.io" get by default in configtrib.emailcontact or server ;emailcontact
tplemail.to="<{{emailcontact}}>"
tplemail.subject="Vos clés d'identification pour {{alias}}"
tplemail.cc=""
tplemail.bcc=""
tplemail.attachments=[]
/*example 
 attachments: [
    {
      filename: "{{data}}.txt",
      pathfile: "{{data}}",
      contenttype: "text/plain",
      minepart: "mixed",
    },
    {
      filename: "my-image.jpg",
      content: "blabla content",
      contenttype: "image/jpeg",
      minepart: "alternative",
    },
  ],
  if content and filepath is present filepath content replace content
  */
tplemail.html=`
<html>
    <head>
        <meta charset="utf-8">
        <style>
        </style>
    </head>
    <body>
    <table cellpadding="0" cellspacing="0" align="center" style="width: 600px; border: none; padding: 0;" width="600">
        <tr style="padding:20px;">
            <td>
                <p style="padding:20px 0 5px 0;">
                <img width="200px" src="https://smatchit.io/static/img/logo/logoBlackSimple.webp" srctmp="https://smatchit.io/trk/static/img/logo/logoBlackSimple.webp?alias={{alias}}&uuid={{uuid}}&srckey=registeremailcontact&version=1&consentcookie={{consentcookie}}" alt="smatchit" />
                </p>
                <hr style="border-top:1px solid #0DC3FF;">
            </td>    
        </tr>
        <tr>
            <td style="padding:20px 0 40px 0;">
                <h1 style="font-size:15px;font-family:Monserrat;">Votre identité numérique: {{alias}}</h1> 
                <p style="font-size:12px;font-family:Monserrat;">Vous avez souhaité recevoir vos clés d'identification via {{tribe}}. Une identité numerique est composée de 2 fichiers texte appelés clé privée et clé public. </p>
                <p style="font-size:12px;font-family:Monserrat;">
                Son usage est simple mais souvent méconnu, avec une fonction mathematique, la clé privée permet de signer un message. Avec une autre fonction, on vérifie que la signature est bien issue de la cle public sans avoir besoin de disposer de la clé privée. 
                </p>
                <p style="font-size:12px;font-family:Monserrat;">Les applications de {{tribe}} fonctionnent toutes de cette façon et permettent non seulement de vous identifier mais aussi de chiffrer vos données lisible uniquement grâce à votre clé privée. Pour simplifier l'usage de ces clés nous associons un alias à votre clé public.</p>
                {{#avecpassphrase}}
                <p style="font-size:12px;font-family:Monserrat;">
                Pour plus de securité, on peut vous demander une passphrase qui dans votre cas est :{{passphrase}}
                </p>
                {{/avecpassphrase}}
                <p style="font-size:12px;font-family:Monserrat;">Gardez précieusement ces clés et <strong>ne communiquez jamais votre clé privée</strong>. Uniquement votre alias (et eventuellement votre clé public).</p>

            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" align="center" style="width: 100%; border: none; padding: 0;" width="100%">    
        <tr bgcolor="#161616">
            <td bgcolor="#161616" align="center" style="padding:20px;">
                <p style="text-align:center;">
                    <img src="https://smatchit.io/static/img/logo/logoSignature.webp" alt="smatchit">
                </p>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" align="center" style="width: 600px; border: none; padding: 0;" width="600" >    
        <tr>
            <td style="padding:20px 0 0 0;">
                <p style="font-size:11px;font-family:Monserrat;">
                    Nos conditions d'utilisation et politique de protection des données <a href="https://smatchit.io/cgu_fr.html" target='_blank'> CGU</a>
                </p>
            </td>
        </tr>
    </table>
    </body>
</html>
`
tplemail.text=`
Vos clés d'authentifications:
alias:      {{alias}}
passphrase: {{passphrase}}
publickey: copier coller entre les 2 lignes -------
---------------------
{{publickey}}
---------------------
privatekye: copier coller entre les 2 lignes -------
---------------------
{{privatekey}}
---------------------
`
module.exports=tplemail;