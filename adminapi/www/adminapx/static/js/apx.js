/*eslint no-undef:0*/
/*eslint-env browser*/

"use strict";
var apx = apx || {};

/**
 * Js lib to interact with apxtrib backend
 *
 * html page must have apxlocal with at least headers key
 *   <script>
      const apxlocal={
       headers:{xalias:"",xhash:"",xtribe:"smatchit", xapp:"smatchapp", xlang:"en" },
       firstimeload:true, 
       forceload:true,
      tpl:{
          footer:"{{{msg}}}"
      },
      tpldata:{
        footer:{msg:"<p>Made with love for people freedom, enjoy!</p>"}
      },
      objects:{}
    };
 * apx.ready(callback) equivalent of jquery Document.ready()
 * apx.save() save apx.data as xapp value in localStorage
 * apx.update() get localStorage up to date
 * apx.loadfile({url:name},dest,axiosoptions) async that wait to load a list of relativ url and store it in a apx.data[dest] 
 */

apx.ready = (callback) => {
  /**
   * Wait loading DOM, when ready run callback function
   */
  if (!callback) {
    alert(
      "You have an unknown callback pwa.state.ready(callback), you need to order your code callback = ()=>{} then pwa.state.ready(callback), boring but js rules ;-)"
    );
  }
  if (document.readyState != "loading") callback();
  // modern browsers
  else if (document.addEventListener)
    document.addEventListener("DOMContentLoaded", callback);
  // IE <= 8
  else
    document.attachEvent("onreadystatechange", function () {
      if (document.readyState == "complete") callback();
    });
};

apx.loadfile = async (list, dest, axiosoptions) => {
  /**
   * Load external file as tpl, tpldata (temmplate and data), objects index of object
   * @param {object} list {name:url} it will run as promise all the requested url
   * @param {string} dest where to store it in apx.data (tpl, tpldata, objectname,...)
   * @param {object} axiosoptions alow to add headers any option to axios
   * @return {object} load into apx.data[dest]={name:{object}} and localStorage.xapp[dest]={name:{object}}
   *
   * @example <caption>Load into apx.data.tpl.blocncol ="string"</caption>
   * apx.loadfile({blocncol:"static/tpl/blocncol.mustache"},"tpl",{headers:apx.data.header})
   *
   */
  const invertlist = {};
  Object.keys(list).forEach((n) => {
    invertlist[list[n]] = n;
  });
  //console.log( 'invertlist', invertlist )
  if (
    apx.data.forcereload ||
    !apx.data[dest] ||
    Object.keys(apx.data[dest]).length == 0
  ) {
    if (!apx.data[dest]) apx.data[dest] = {};
    const fetchURL = (r, options) => {
      return axios.get(r, options);
    };
    const urls = Object.keys(invertlist);
    if (!axiosoptions) {
      // force no cache browser
      apx.data.headers["Cache-Control"] = "no-cache";
      apx.data.headers["Pragma"] = "no-cache";
      apx.data.headers["Expires"] = 0;
      axiosoptions = { headers: apx.data.headers };
    }
    console.log("axiosiptions", axiosoptions);
    const promiseArray = urls.map((r) => fetchURL(r, axiosoptions));
    await Promise.all(promiseArray)
      .then((data) => {
        console.log(data);
        for (let pos = 0; pos < urls.length; pos++) {
          //console.log( 'url', urls[ pos ] )
          //console.log( data[ pos ] );
          apx.data[dest][invertlist[urls[pos]]] = data[pos].data;
          console.log(
            `store apx.data[ ${dest} ][ invertlist[ ${urls[pos]} ] ]`
          );
          apx.save();
        }
      })
      .catch((err) => {
        console.log("erreur de loading", err);
      });
  }
};

apx.save = () => {
  localStorage.setItem(apx.data.headers.xapp, JSON.stringify(apx.data));
};
apx.update = async () => {
  if (!apxlocal) {
    console.log(
      'Please add to the html page header, this line  <script> const apxlocal={headers:{xalias:"",xhash:"",xtribe:"smatchit", xapp:"smatchapp", xlang:"en" }};</script> '
    );
    return;
  }
  if (document.querySelector("html").getAttribute("lang")) {
    apxlocal.headers.xlang = document
      .querySelector("html")
      .getAttribute("lang");
  }
  if (localStorage.getItem(apxlocal.headers.xapp)) {
    apx.data = JSON.parse(localStorage.getItem(apxlocal.headers.xapp));
    if (apx.data.headers.xtribe != apxlocal.headers.xtribe) {
      // if an app change of tribe
      localStorage.removeItem(apxlocal.headers.xapp);
      delete apx.data;
    }
  }
  if (!apx.data) {
    apx.data = apxlocal;
  }
  console.log("apx.data", apx.data);
  apx.save();
};
apx.ready(apx.update);
