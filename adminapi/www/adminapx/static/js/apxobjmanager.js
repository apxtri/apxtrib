/*eslint no-undef:0*/
/*eslint-env browser*/

"use strict";
var objman = objman || {};


objman.objedit = (idcomponent,id) => {
  const objdoc = document.getElementById(idcomponent);
  objdoc.querySelector(".objlist").classList.add("d-none");
  objdoc.querySelector(".objitm").classList.remove("d-none");
  objdoc.querySelector(".objaddedit").classList.add("hide");
  objdoc.querySelector(".inputfilter").classList.add("hide");
  objdoc.querySelector(".btnfilter").classList.add("hide");
  objdoc.querySelector(".objlistreturn").classList.remove("d-none");
  //recupere les data et le schema
};
objman.returnlist = (idcomponent) => {
  const objdoc = document.getElementById(idcomponent);
  objdoc.querySelector(".objlist").classList.remove("d-none");  
  objdoc.querySelector(".objitm").classList.add("d-none");
  objdoc.querySelector(".objaddedit").classList.remove("hide");
  objdoc.querySelector(".inputfilter").classList.remove("hide");
  objdoc.querySelector(".btnfilter").classList.remove("hide");
  objdoc.querySelector(".objlistreturn").classList.add("d-none");
};

