# apXtrib a Decentralized Autonomous Organisation (DAO)

You are here on the tech side, to understand under the wood how it works and how you can contribute to this tech journey.
See [apXtrib web site](https://apxtrib.crabdance.com) how to create a new social world with apXtrib.

## apXtrib Architecture a quick view & keywords definition

- **a nation** is a topology network of nodes town that share the same fundamental rules (engrave into a common blockchain). 
- **a town** is a physical installation node of a machine (linux server) plug into a LAN network (a telecom box NAT conf) accessible into internet a WAN network (domain name redirect to a town public IP).
- **a tribe** has a unique name and is a space to host data to apply specific algo rule register at the creation of a tribe by a druid that respect the town rules 
- **a pagan** 'village farmer' is a unique identification create by anyone based on PGP (public/Private key) and an alis (humanly understandnable) that allow a member to proof that he owns data. Can be anonymous but all his actions are store into a blockchain of reputation.
- **a druid** 'rules master' is a pagan that owned and defined rules of a tribe, he owns a domain name (DNS) to make it tribe's data accessible over the net. As pagan he can be anonymous, he is responsible of the tribe activities and can ban a pagan of his tribe. Any pagan become a druid when he decides to create a tribe by requesting a town mayor
- **a mayor** is a owner of a physical machine, he is in charge to make a town node working (IP availibily) and accept to be part of a nation (rules of the blockchain nation).
- **a contract** is an algorythme that is written into the blockchain and will be run if an action trig. Immutable contract are the one link to the blockchain. Any pagan/druid/major can defined a contract that apply to other pagans.
- **the XTRIB coin** is the token that drive the blockchain, it materialises an exchange value and is a conterpoint
- **a git apxtrib** is a package ready to install a town by a mayor into a physical server that will be able to join a nation (mean accept thoses nation rules). You can also create a nation and stay alone or not. Then mayor will be able to host druid request to create tribe. Then druid will invite pagan to join his tribe...

All actors will have the same target to respect contracts and are free to leave or to stay into a nation, a town or a tribe. If a contract is not fair, then a nation, tribe, will be empty mean not creating value mean die. Only fair rules will survive but any try will rewards actor with XTRIB coin.

## Network topology

As it is decentralize organisation, a public address directory is replicated on each instance of a town into /nationchains .<br>
Each town (instance) is accessible with an anonymlous DNS https://apxtrib.crabdance.com where the IP adresse is update when elected town's IP to register a new block into the blockchain (each 10minutes).

```
/nationchains
               /blocks #the blockchain each file is a block
               /socialword
                        /contracts/           #sign contracts each js file is a rules register in the blockchain
                        /metaobject/          #sign meta data under blockchain control to validate object integrity (pagans, druid, contract,town, nation, ...)
                        /objects/name/             # uuid.json object description content
                                     /searchindex/ # index files to apply CRUD in an easy way
               /static/ any usefull static file
               apxtrib.html   # Describe the project as a end-user
               namennation.html # ex: antsnation.html describe the rules for this nation and explain benefit for a futur mayor to join it
          
```



## apXtrib Documentation 

**Documentation for apXtrib usage to start**, tuto HOW TO for user, dev ... is [here](https://apxtrib.org/doc) 
- [Set up](https://gitea.ndda.fr/apxtrib/apxtrib/wiki/Setup)
- [Manage users](http://gitlab.ndda.fr/philc/apxtrib/-/wikis/HOWTOuser)
- [Manage multilangue referentials](http://gitlab.ndda.fr/philc/apxtrib/-/wikis/HOWTOreferential)
- [How to manage trigger action](http://gitlab.ndda.fr/philc/apxtrib/-/wikis/HOWTOtriggeraction)

To quickly test we recommand you to "Set up for dev", you just need an linux desktop and it rocks...
Read more on the wiki's project [apxtrib wiki](http://gitlab.ndda.fr/philc/apxtrib/-/wikis/home)


**Documentation for apXtrib dev** is done into the gitea wiki project.
You will find anything to understand how it works under the wood. Feel free to ask question, comment to improve the code. If you need to debug or you want to add a main feature setup your machine as local (no exchange with the blockchain)

- [Nations -> Towns -> Tribes -> Pagans Architecture](https://gitea.ndda.fr/apxtrib/apxtrib/wiki/HardwareNation) the social world
- [nationchains]() the blockchain and social contracts between mpayor, druids and pagans.
- [package.json and tribes/townconf.js]() setup and convention rules: eslint, unit test, multi-lang, header
- [middleware/route/Models]() restfullapi logical from routes to middleware to models to datamapper and result res.status(http code).json({data,info,moreinfo})
- [accessrights]() Access right definition on object (Object,...)
- [Odmdb.js]() convention for object descriptor manager to index, search, create, update, delete item of an Object collections (to simplify CRUD actions for Models)
- [Pagans.js]() decentralized and anonymous publicKey for a pagan based on PGP with accessright onto other objects. Authentification process and localStorage management of 24 hours JWT. Login/psw/email. 
- [Tribes.js]() A Druid space to host a web space. 
- [Tags.js]() Event trackers to log any usefull event that can be used into sodial contracts
- [Messages.js]() Notification (delete after read) and message between any interactions concerning pagans (1 to many or 1 to 1), trig by social contract or by a pagan
  [Setup.js]() Technical infrastructure an
- [Blockchains and Contracts]() Contracts.js

**ROADMAP**

- Setup process to review to simplify Town setup dev and setup prod that create 1 tribe with name town, with 1 pagan set with the role of druid of the tribe and mayor of the town. 
- Decentralized social Data model  based on schema.org + data type definition and integrity check  + metadata
- GUI for mayor to create new tribe and manage druid and payment space (based on bitcoin wallet)
- GUI for druid to manage space storage (publish a web site, manage media storage)
- Plugins process to share tribe route / Model for specific app
- Decentralized social Contracts nationschains in socialworld/contracts list of contracts